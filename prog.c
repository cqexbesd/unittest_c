#include <dlfcn.h>
#include <stdbool.h>
#include <stdio.h>

#include "ut.h"

bool foo(void);

UNITTEST {
	printf("running one\n");

	return true;
}

bool foo(void) {

	printf("foo\n");

	return true;
}

UNITTEST {
	printf("running two\n");

	return false;
}

#ifndef UNITTEST_RUN_TESTS
int main(int argc, char *argv[]) {

	printf("in main main\n");

	return 0;
}
#endif
