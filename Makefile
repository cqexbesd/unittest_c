PROG=prog
SRCS=${PROG}.c ut.h ut.c
CSTD=gnu2x
CFLAGS+=
LDADD+=

.ifdef(DEBUG)
DEBUG_FLAGS+=-O0 -g
.endif

.ifdef(M32)
CFLAGS+=-m32
.endif

WARNS=9
NO_WERROR=true

NO_MAN=true

.ifmake unittest
CFLAGS+=-DUNITTEST_RUN_TESTS
.endif

unittest: ${PROG}
	mv ${PROG} ${PROG}.unittest

.include <bsd.prog.mk>
