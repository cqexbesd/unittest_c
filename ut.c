#include <sys/queue.h>

#include <err.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "ut.h"
#include "ut_private.h"

static SLIST_HEAD(UNITTEST_list_head, UNITTEST_descriptor) test_list_head =
										SLIST_HEAD_INITIALIZER(test_list_head);

static void UNITTEST_usage(const char *name) {
	fprintf(stderr, "usage: %s [-v]\n"
					"\n"
					"-v	verbose. print all tests run\n"
					"\n"
					"run unit tests\n", name);
}

void UNITTEST_register_unittest(bool (* test)(void), const char *file, const unsigned int line) {
	struct UNITTEST_descriptor *le;

	if ((le = malloc(sizeof(*le))) == NULL) {
		err(1, "malloc failed allocating space for unittests");
	}

	le->test = test;
	le->file = file;
	le->line = line;

	SLIST_INSERT_HEAD(&test_list_head, le, next);
}

int UNITTEST_main(int argc, char *argv[]) {
	int c;
	bool verbose;
	int failures = 0;
	struct UNITTEST_descriptor *descriptor;

	while ((c = getopt(argc, argv, "hv")) != -1) {
		switch (c) {
			case 'v':
				verbose = true;
				break;
			default:
				UNITTEST_usage(argv[0]);
				return 1;
				break;
		}
	}

	SLIST_FOREACH(descriptor, &test_list_head, next) {
		if (! descriptor->test()) {
			failures++;
			if (failures >= 256) {
				failures = 1;
			}
			fprintf(stderr, "failed unittest at %s:%u\n", descriptor->file,
					descriptor->line);
		} else if (verbose) {
			fprintf(stderr, "passed unittest at %s:%u\n", descriptor->file,
                    descriptor->line);
		}
	}

	return failures;
}

__attribute__((weak)) int main(int argc, char *argv[]) {
	return UNITTEST_main(argc, argv);
}
