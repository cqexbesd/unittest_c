#ifndef UT_PRIVATE_H
#define UT_PRIVATE_H

#include <sys/queue.h>

struct UNITTEST_descriptor {
	bool (* test)(void);
	const char *file;
	unsigned int line;

	SLIST_ENTRY(UNITTEST_descriptor) next;
};

void UNITTEST_run_unittests(void);

#endif
