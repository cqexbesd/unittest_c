#ifndef UT_H
#define UT_H

#ifdef UNITTEST_RUN_TESTS
#define UNITTEST_ATTR constructor
#else
#define UNITTEST_ATTR unused
#endif

#define UNITTEST UNITTEST_impl1(__COUNTER__)

#define UNITTEST_impl1(c)	UNITTEST_impl(c)

#define UNITTEST_impl(count)	\
	void UNITTEST_register_ ## count(void);	\
	bool UNITTEST_impl_ ## count(void);     \
											\
	__attribute__((UNITTEST_ATTR)) void UNITTEST_register_ ## count(void) {	\
											\
		UNITTEST_register_unittest(&UNITTEST_impl_ ## count, __FILE__, __LINE__);	\
	}										\
											\
	bool UNITTEST_impl_ ## count(void)

void UNITTEST_register_unittest(bool (* test)(void), const char *file, const unsigned int line);
int UNITTEST_main(int argc, char *argv[]);

#endif
